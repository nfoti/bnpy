# Keyword options for observation models
#  specify how to set prior on data-generating parameters

[ZMGauss]
min_covar=1e-8
dF=0
sF=1.0
ECovMat=eye


[ZMGaussHelp]
min_covar=Minimum value for diagonal entries of covariance matrix, to ensure invertibility (EM only). Set very small to approach maximum likelihood estimates.
dF=Number of degrees of freedom for Wishart prior. Must be >= dimension of Data.
sF=Scale factor for expected covariance matrix under Wishart prior. Set very small to approach maximum likelihood estimates.
ECovMat=Name of routine for setting expected covariance matrix under Wishart prior. Options: {'eye', 'covdata'}.

[Gauss]
min_covar=1e-8
dF=0
sF=1.0
ECovMat=eye
kappa=1e-4

[GaussHelp]
min_covar=Minimum value for diagonal entries of covariance matrix, to ensure invertibility [algName='EM']. Set very small to approach maximum likelihood estimates.
dF=Number of degrees of freedom for Wishart prior. Must be >= dimension of Data.
sF=Scale factor for expected covariance matrix under Wishart prior. Set very small to approach maximum likelihood estimates.
ECovMat=Name of routine for setting expected covariance matrix under Wishart prior. Options: {'eye', 'covdata'}.
kappa=Scalar that controls the precision (inverse variance) of Gaussian prior on means: \mu[k] ~ Normal( 0, 1/kappa * ECovMat). Set very small to allow means to approach maximum likelihood estimates.

[Mult]
lambda=0.01

[MultHelp]
lambda=parameter for symmetric Dirichlet prior over each topic's word distribution
